# sqlserver2012在非windows域网络环境下高性能数据库镜像模式的设置

#### 1. 前言

本项目所提供的是非windows域的网络环境下，以高性能模式运行的主/镜像sqlserver数据库所需要的基本设置工作。设置完成后两台sqlserver数据库实例应该可以执行数据库镜像操作。

许多情况下,sqlserver数据库镜像是在非windows域环境下的，在这种环境下需要通过配置证书才能正确的互相连通执行镜像。

生产环境下，sqlserver的镜像机制至少需要两个数据库实例。一般来说需要两台数据库服务器,一台持有使用的数据库，

称为**主数据库**(也称为**主体数据库实例**),该服务器本文档称为**主服务器**，一台持有镜像数据库使用，称为**镜像数据库**,该服务器称为**镜像服务器**。数据库镜像可以使用两种模式:**高性能模式**和**高安全模式**。

   设置完成后，仍需要其他操作才能进行一个完整的数据库镜像。

   **高性能模式**和**高安全模式**的区别，请参考:`[sqlserver数据库镜像的模式][2]`

#### 2. 运行环境

   1. 要求主服务器和镜像服务器必须安装相同版本的sqlserver。适应的sqlserver 版本:2012。其他的sqlserver版本除非过于古老，应该也可以，但没有测试过。

   2. 使用者必须安装[jupyter](https://jupyter.org "jupyter笔记本官网")笔记本环境，推荐直接安装[Anaconda](https://www.anaconda.com "anaconda官网")

   要求使用者具有使用*jupyter笔记本*的知识。


   3. Annacoda环境或者python环境要求安装pymssql模块，jinjia2模块。

#### 3. 如何使用

   确定满足条件后，下载整个项目目录到磁盘，然后使用jupyter笔记本工具(推荐 visual code)打开该目录，打开该目录下的setup_sqlserver.ipynb笔记本文件，按提示执行操作。

   基本上，从步骤1开始每一步的代码单元格执行完后，都会产生一个sql文件，用sa用户执行该sql文件。依次运行代码单元格，并完成相应的操作。全部完成后，应该可以执行数据库镜像了。

   该笔记本最后也简单阐述了如何当主服务器发生问题时如何处理使得镜像服务器变得可用，以及数据库镜像的优缺点和适用范围。

#### 4. 执行的操作

   笔记本所产生的sql文件将执行以下操作。

   **(1) 步骤1**

   这一步是进行主服务器的出站(outbound)设置。将在主数据库里：

* 如果主加密密钥不存在，将创建主加密密钥
* 如果名为master_cert的证书不存在，将创建该证书
* 如果名为Endpoint_Mirroring的数据库镜像端点不存在，将创建该端点

  确保 c:\cer\master_cert.cer文件在脚本执行前不存在。脚本执行完成后，将该文件拷贝到镜像服务器上。
  c:\cer\是假定的证书输出位置,可以在文件中修改成其他位置。

  如果不是初始化，可以需要根据实际情况调整这个脚本。

  新建的数据库镜像端点Endpoint_Mirroring使用默认的端口号**5022**，可以调整为其他端口号，并注意在防火墙允许该端口接受连接。

  如果脚本执行正确完成，将产生c:\cer\master_cert.cer文件。将该文件拷贝到镜像服务器下的c:\cer\master_cert.cer。

  **(2) 步骤2**

  这一步是进行镜像服务器的出站(outbound)设置。将在镜像数据库里：
* 如果主加密密钥不存在，将创建主加密密钥
* 如果名为mirror_cert的证书不存在，将创建该证书
* 如果名为Endpoint_Mirroring的数据库镜像端点不存在，将创建该端点。

  确保 c:\cert\mirror_cert.cer文件执行前不存在，完成后，将该文件拷贝到主服务器上,
  c:\cert\是假定的证书输出位置

  如果不是初始化，可能需要根据实际情况调整这个脚本。

  新建的数据库镜像端点Endp1oint_Mirroring使用端口号**5023**，可以调整为其他端口号，

  ***!!!注意不要和主数据库镜像端点的端口号重复!!!***

  并注意在防火墙允许该端口接受连接。

  **(3) 步骤3**

  这一步是进行主服务器的入站(inbound)设置。

  确保主服务器和镜像服务器的出站设置已成功执行,并且在主服务器的c:\cer\mirror_cert.cer文件存在，该文件是
  镜像服务器的出站设置产生的安全证书文件。

  将在主数据库删除:
* 名为mirror_cert的证书
* 名为mirror_user的用户
* 名为mirror_login的登录名

  将在主服务器产生:
* 从c:\cer\mirror_cert.cer证书文件，产生名为mirror_cert的证书
* 产生名为Mirror_Login的登录名，并设置好自己的**登录密码**
* 产生名为Mirror_User的用户，并作为Mirror_Login的映射用户

**(4) 步骤4**
   这一步是进行镜像服务器的入站(inbound)设置。

   将在镜像服务器上删除:

* 名为master_cert的证书
* 名为master_user的用户
* 名为master_login的登录名

  将在镜像服务器产生:
* 从c:\cer\master_cert.cer证书文件，产生名为master_cert的证书
* 产生名为Host_Login的登录名，并设置好自己的**登录密码**
* 产生名为Host_User的用户，并作为Host_Login的映射用户

 **小结**

    请严格按setup_sqlserver.ipynb上提示的顺序执行单元格并以管理员身份执行产生的SQL文件，并在步骤1，步骤2后复制产生的证书文件到对方服务器上。正确的执行完毕后参考setup_sqlserver.ipynb笔记本后面的文档。

#### 5. 文件内容

```
目录
   ├── README.md(本文档)
   ├── LICENSE.txt
   ├── setup_sqlserver.ipynb (运行这个笔记本)
   ├── templtes\ (使用的jiaja2模板)
   └── helper.py (辅助方法)
```

#### 6. 参考

[1]: sqlserver数据库镜像的文档 [微软官方文档](https://docs.microsoft.com/zh-cn/sql/database-engine/database-mirroring/database-mirroring-sql-server?view=sql-server-ver15)

[2]: sqlserver数据库镜像的模式 [微软官方文档](https://docs.microsoft.com/zh-cn/sql/database-engine/database-mirroring/database-mirroring-sql-server?view=sql-server-ver15#OperatingModes)

[3]: 基于证书的数据库镜像 [微软官方文档](https://docs.microsoft.com/zh-cn/sql/database-engine/database-mirroring/example-setting-up-database-mirroring-using-certificates-transact-sql?view=sql-server-ver16)
